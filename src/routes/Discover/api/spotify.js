import makeRequest from './makeRequest';

export const getCategories = () => {
    return makeRequest('categories', 'categories');
}

export const getFeaturedPlaylists = () => {
  return makeRequest('featured-playlists', 'playlists');
}

export const getNewReleases = () => {
  return makeRequest('new-releases', 'albums');
}
